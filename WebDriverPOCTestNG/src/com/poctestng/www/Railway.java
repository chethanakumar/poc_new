package com.poctestng.www;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class Railway

{
// Adding external comment

	@Test //This is TestNG annotation

	public void testRegister()

	{

		WebDriver wd=new FirefoxDriver();
		wd.get("http://www.indianrail.gov.in/");
		
		wd.manage().window().maximize();
		//Thread.sleep(6000);
		
		String tabCss="a[href='between_Imp_Stations.html']";
		String srcStationCss="select[name='lccp_src_stncode'] option";
		String destStationCss="select[name='lccp_dstn_stncode'] option";
		String classCss="select[name='lccp_classopt'] option";
		String getBtnCss="input[value='Get Details']";
		
		WebElement tabEle=wd.findElement(By.cssSelector(tabCss));
		tabEle.click();
		
		List<WebElement> srcStnList=wd.findElements(By.cssSelector(srcStationCss));
		int srcListCount=srcStnList.size();
		for(int i=0;i<srcListCount;i++)
		{
			String stationName=srcStnList.get(i).getText();
			if(stationName.equals("AGRA CANTT - AGC"))
			{
				srcStnList.get(i).click();
			}
		}
		
		System.out.println("source station selected");
		
		List<WebElement> dstStatnDropDown=wd.findElements(By.cssSelector(destStationCss));
		int dstCountList=dstStatnDropDown.size();
		
		for(int i=0;i<dstCountList;i++)
		{
			String stationName=dstStatnDropDown.get(i).getText();
			if(stationName.equals("BANGARAPET - BWT"))
			{
				dstStatnDropDown.get(i).click();
			}
		}
		
		System.out.println("Destination station selected");
		
		List<WebElement> classDropDown=wd.findElements(By.cssSelector(classCss));
		int classDropDownListCount=classDropDown.size();
		
		for(int i=0;i<classDropDownListCount;i++)
		{
			String className=classDropDown.get(i).getText();
			if(className.equals("FIRST AC"))
			{
				classDropDown.get(i).click();
			}
		}
		
		System.out.println("Class in train selected");
		
		WebElement getBtn=wd.findElement(By.cssSelector(getBtnCss));
		getBtn.click();
		
		System.out.println("Submit button");

		wd.close();

//		wd.quit();

}

}